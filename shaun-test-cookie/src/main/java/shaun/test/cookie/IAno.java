package shaun.test.cookie;

import java.lang.annotation.*;

/**
 * @author miemie
 * @since 2020-06-10
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface IAno {
}

package com.baomidou.shaun.core.enums;

/**
 * @author miemie
 * @since 2019-08-08
 */
public enum Logical {
    BOTH, ANY
}
